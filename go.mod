module gitlab.bcowtech.de/bcow-go/worker-nsq

go 1.14

require (
	github.com/golang/snappy v0.0.2 // indirect
	github.com/nsqio/go-nsq v1.0.8
	gitlab.bcowtech.de/bcow-go/config v1.2.1
	gitlab.bcowtech.de/bcow-go/host v1.10.4
	gitlab.bcowtech.de/bcow-go/structprototype v1.3.0
)
